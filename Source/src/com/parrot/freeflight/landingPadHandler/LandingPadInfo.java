package com.parrot.freeflight.landingPadHandler;

public class LandingPadInfo {
	
	public boolean detectLandingPad;
	public boolean landingPadDetected;
	public boolean downwardCameraOn;
	
	public LandingPadInfo()
	{
		detectLandingPad = false;
		landingPadDetected = false;
		downwardCameraOn = false;
	}

}
