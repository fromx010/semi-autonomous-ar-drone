package com.parrot.freeflight.landingPadHandler.Detection;

import android.content.Context;

public class LandingPadDetector {
	
	private volatile static LandingPadDetector instance;
	private Context applicationContext;
	
	public static LandingPadDetector getInstance(Context appContext)
	{
		if (instance == null) {
			instance = new LandingPadDetector(appContext);
		} else {
		    instance.setAppContext(appContext);
		}

		return instance;
	}


    public LandingPadDetector(Context appContext)
	{
		
	}
    
    private void setAppContext(Context appContext)
    {
        this.applicationContext = appContext;
    }

}
