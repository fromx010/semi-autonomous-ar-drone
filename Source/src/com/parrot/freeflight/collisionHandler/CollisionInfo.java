/**
 * <p>
 * A Java Object to store collision state information, such as optical flow map, 
 * and telemetry data (speed, accl, magneto). 
 * </p>
 * <p>
 * It is managed by the collision detection classes to store state information relevant
 * to the collision avoidance routines to generate paths and instructions to avoid the
 * detected collisions.
 * </p>
 * <p>
 * Populated by: com.parrot.freeflight.collisionHandler.Detection package <br />
 * Read By: com.parrot.freeflight.collisionHandler.Avoidance <br />
 * </p>
 * 
 * @author fromx010
 */
package com.parrot.freeflight.collisionHandler;


/**
 * @author fromx010
 *
 */
public class CollisionInfo {
	
	public boolean avoidCollisions;
	public boolean collisionDetected;
	public boolean forwardCameraOn;
	
	public CollisionInfo()
	{
		avoidCollisions = false;
		collisionDetected = false;
		forwardCameraOn = true;
	}

}
