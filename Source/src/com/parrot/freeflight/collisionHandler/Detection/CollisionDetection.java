package com.parrot.freeflight.collisionHandler.Detection;

import com.parrot.freeflight.collisionHandler.CollisionInfo;
import com.parrot.freeflight.service.commands.DroneCommand;
import com.parrot.freeflight.service.commands.DroneMovementCommand;

import android.content.Context;

/**
 * <p>
 * Handles detection of collisions based on video and telemetry data.
 * </p>
 * <p>
 * All collisionInfo object property changes should be executed
 * by this class only.
 * </p>
 * @author fromx010
 */
public class CollisionDetection {
	
	private volatile static CollisionDetection instance;
	private Context applicationContext;
	
	public static CollisionDetection getInstance(Context appContext)
	{
		if (instance == null) {
			instance = new CollisionDetection(appContext);
		} else {
		    instance.setAppContext(appContext);
		}

		return instance;
	}


    public CollisionDetection(Context appContext)
	{
		
	}
    
    private void setAppContext(Context appContext)
    {
        this.applicationContext = appContext;
    }
    
    /**
     * <p>
     * Checks to see if the requested movement will cause a collision.
     * It will set collisionDetected in CollisionInfo appropriately.
     * </p>
     * 
     * @param movementCommand the movement the user wants to make
     */
    public boolean willCollide(DroneMovementCommand movementCommand, CollisionInfo collisionInfo)
    {
    	boolean collisionDetected = false;
    	/*
    	 * TODO see if the command will cause a collision set collisionDetected
    	 * and return a boolean if there will be a collision
    	 */
    	//to make the compiler happy
    	
    	collisionInfo.collisionDetected = collisionDetected;
    	return collisionDetected;
    }
    
    public void clearCollisionDetected(CollisionInfo collisionInfo)
    {
    	collisionInfo.collisionDetected = false;
    }
}
