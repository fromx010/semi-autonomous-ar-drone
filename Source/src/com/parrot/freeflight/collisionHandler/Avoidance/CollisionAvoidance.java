/**
 * <p>
 * Contains classes that handle collision avoidance.
 * </P>
 * 
 * @author fromx010
 */
package com.parrot.freeflight.collisionHandler.Avoidance;

import java.util.LinkedList;
import java.util.Queue;

import android.content.Context;

import com.parrot.freeflight.collisionHandler.CollisionInfo;
import com.parrot.freeflight.service.commands.DroneCommand;

/**
 * <p>
 * This class utilizes the CollisionInfo class to generate paths
 * and flight commands to avoid detected collisions.
 * </p>
 * 
 * @author fromx010
 *
 */
public class CollisionAvoidance {
	
	private volatile static CollisionAvoidance instance;
	private Context applicationContext;
	
	public static CollisionAvoidance getInstance(Context appContext)
	{
		if (instance == null) {
			instance = new CollisionAvoidance(appContext);
		} else {
		    instance.setAppContext(appContext);
		}

		return instance;
	}


    public CollisionAvoidance(Context appContext)
	{
		
	}
    
    private void setAppContext(Context appContext)
    {
        this.applicationContext = appContext;
    }
	
    /**
     * <p>
     * Uses the collisionInfo object to generate a path
     * to avoid the collision.
     * </p>
     * 
     * @return A Linked List of commands to execute to avoid
     * 			the collision.
     * 
     * @author fromx010
     */
	public Queue<DroneCommand> generateAvoidancePath(CollisionInfo collisionInfo)
	{
		Queue<DroneCommand> avoidanceCommandQueue = new LinkedList<DroneCommand>();
		//TODO call sub methods to read collisionInfo to generate instruction list
		return avoidanceCommandQueue;
	}
	
	private void addToQueue(Queue<DroneCommand> avoidanceCommandQueue, DroneCommand command)
	{
		avoidanceCommandQueue.add(command);
	}

}
