package com.parrot.freeflight.service.commands;

import com.parrot.freeflight.drone.DroneProxy;
import com.parrot.freeflight.service.DroneControlService;

/**
 * Command object for the commandQueue to execute a flip
 * 
 * @author fromx010
 */
public class FlipCommand 
	extends DroneCommand
	implements DroneMovementCommand
{
	
	private DroneProxy droneProxy;
	 
	 /**
	  * Constructor for the ControlValue command to be added to the command queue.
	  * 
	  * @param context - 
	  * 
	  * @author fromx010
	  */
	 public FlipCommand(DroneControlService context)
	 {
		 super(context);
	     droneProxy = DroneProxy.getInstance(context.getApplicationContext());
	 }
	 
	 @Override
	 /* 
	  * (non-Javadoc)
	  * @see com.parrot.freeflight.service.commands.DroneServiceCommand#execute()
	  */
	 public void execute()
	 {
		 droneProxy.doFlip();
	 }
	 
	 /**
	  * <p>
	  * Get the direction this command moves us.
	  * </p>
	  * @return The enum name.
	  * 
	  * @author fromx010
	  */
	 public String getMovementDirection()
	 {
		 return "Flip";
	 }

}
