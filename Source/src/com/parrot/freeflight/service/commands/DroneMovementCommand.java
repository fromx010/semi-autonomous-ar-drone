package com.parrot.freeflight.service.commands;

/**
 * <p>
 * All DroneServiceCommands that execute movement should implement this
 * interface. The methods defined in this interface are used by the
 * collision avoidance path finding algorithms.
 * </p>
 * 
 * @author fromx010
 *
 */
public interface DroneMovementCommand {
	
	public String getMovementDirection();
}
