/*
 * DroneServiceCommand
 * 
 * Created on: May 5, 2011
 * Author: Dmytro Baryskyy
 */

package com.parrot.freeflight.service.commands;

import com.parrot.freeflight.service.DroneControlService;

public abstract class DroneCommand
{
    protected DroneControlService context;


    public DroneCommand(DroneControlService context)
    {
        this.context = context;
    }


    public abstract void execute();

}
