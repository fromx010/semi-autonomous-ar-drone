package com.parrot.freeflight.service.commands;

import com.parrot.freeflight.drone.DroneProxy;
import com.parrot.freeflight.service.ControlMovementEnum;
import com.parrot.freeflight.service.DroneControlService;

/**
 * <p>
 * Command object for the commandQueue to execute setControlValue 
 * commands which are used for maneuvering the drone on all axis.
 * <p>
 * 
 * @author fromx010
 */
public class ControlValueCommand 
	extends DroneCommand
	implements DroneMovementCommand
{
	 private DroneProxy droneProxy;
	 final float power;
	 final int movementDirectionEnum;
	 
	 /**
	  * <p>
	  * Constructor for the ControlValue command to be added to the command queue.
	  * </p>
	  * @param context - 
	  * @param movementDirectionEnum - Enum Defined in jni/common.h that determines how to move
	  * @param power - How fast we should move between -1 and 1
	  * 
	  * @author fromx010
	  */
	 public ControlValueCommand(DroneControlService context, final int movementDirectionEnum, final float power)
	 {
		 super(context);
		 this.power = power;
		 this.movementDirectionEnum = movementDirectionEnum;
	     droneProxy = DroneProxy.getInstance(context.getApplicationContext());
	 }
	 
	 @Override
	 /* Moves the Drone with the instantiated direction and power.
	  * (non-Javadoc)
	  * @see com.parrot.freeflight.service.commands.DroneServiceCommand#execute()
	  */
	 public void execute()
	 {
		 droneProxy.setControlValue(movementDirectionEnum, power);
	 }
	 
	 /**
	  * Get the power assigned to the movement action
	  * 
	  * @return power - How fast we should move between -1 and 1
	  * 
	  * @author fromx010
	  */
	 public float getPower()
	 {
		 return this.power;
	 }
	 
	 /**
	  * <p>
	  * Get the direction this command moves us.
	  * </p>
	  * @return The enum name.
	  * 
	  * @author fromx010
	  */
	 public String getMovementDirection()
	 {
		 /*
		  * values() returns an array containing all of the enum constant names 
		  * in the order they appear. Since we are assigning the ids starting from
		  * 0 like a classic C/C++/C# enum type we can retrieve the enum name by using
		  * the Id to index to the correct one. 
		  */
		 return ControlMovementEnum.values()[this.movementDirectionEnum].toString();
	 }
}
