package com.parrot.freeflight.service;

public enum ControlMovementEnum {
	// These constants should match the enum defined in jni/common.h
	CONTROL_SET_YAW(0),
	CONTROL_SET_GAZ(1),
	CONTROL_SET_PITCH(2),
	CONTROL_SET_ROLL(3);
	
	private final int id;
	
	ControlMovementEnum(int id)
	{
		this.id = id;
	}
	
	public int getEnumValue()
	{
		return id;
	}
}

